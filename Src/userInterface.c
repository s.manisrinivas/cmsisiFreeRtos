/*
 * userInterface.c
 *
 *  Created on: 12-Apr-2020
 *      Author: mani.srinivas
 */
#include "userinterface.h"
#include "cmsis_os.h"
#include "gpio.h"


osTimerId singlePress;
osTimerId longPress;

void singlebuttonPress(void const * argument);
void longbuttonPress(void const * argument);




void UIinterface()
{
	osTimerDef(onePress,singlebuttonPress);
	singlePress=osTimerCreate(osTimer(onePress),osTimerOnce, (void*) 0);
	if(singlePress == NULL)
	{
		printf("Error in creating timer\n");

	}
	osTimerDef(longPress,longbuttonPress);
	longPress=	osTimerCreate(osTimer(longPress),osTimerOnce, (void*) 0);
	if(longPress == NULL)
		{
			printf("Error in creating timer\n");

		}
}



void singlebuttonPress(void const * argument)
{


	if(HAL_GPIO_ReadPin(GPIOC,USER_Btn_Pin)  == GPIO_PIN_SET)
	{
		printf("Single press detected\n");
	}
}

void longbuttonPress(void const * argument)
{

	if(HAL_GPIO_ReadPin(GPIOC,USER_Btn_Pin) == GPIO_PIN_RESET)
	{
		printf("Long press detected\n");
	}
}


void userbuttonPressed()
{
	static int pbLongPress= 700;
	static int  pbInterval = 500;
	osStatus osst;


	if(longPress != 0 && singlePress !=0 )
	{

		if(HAL_GPIO_ReadPin(GPIOC,USER_Btn_Pin) == GPIO_PIN_SET){

//			printf("Timer button callback \n");
			osTimerStart(longPress, pbLongPress);
			if(osst != osOK)
			{
				printf("error timer\n");
			}
		}



		if(HAL_GPIO_ReadPin(GPIOC,USER_Btn_Pin)  == GPIO_PIN_RESET)
		{
			//Button is released
//			printf("Button is released callback \n");
			osst = osTimerStart(singlePress, pbInterval);
			if(osst != osOK)
			{
				printf("error timer\n");
			}

		}
	}
}
